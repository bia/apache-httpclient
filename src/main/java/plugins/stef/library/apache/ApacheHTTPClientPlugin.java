package plugins.stef.library.apache;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Apache HTTP Client library.
 * 
 * @author Stephane Dallongeville
 */
public class ApacheHTTPClientPlugin extends Plugin implements PluginLibrary
{
    //
}
